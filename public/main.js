function calc() {
    let price = document.getElementsByName("price");
    let quantity = document.getElementsByName("quantity");
    let result = document.getElementById("result");
    let re = /\D/;
    if ((price[0].value.match(re) || quantity[0].value.match(re)) === null)
        result.innerHTML = ("стоимость вашего заказа: " + parseInt(price[0].value, 10) * parseInt(quantity[0].value, 10));
    else result.innerHTML = "Ошибка! Формы не должны содержать букв и знаков.";
    return false;
}
